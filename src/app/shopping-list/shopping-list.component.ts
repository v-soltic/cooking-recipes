import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  // before we begin to define what our shopping list will hold, we need to first create a model for it. 
  ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10)
  ];
  // after creating the new objects above, what is next?
        // go inside shopping-list.html
  constructor() { }

  ngOnInit() {
  }

}


/* 
Since ingredients will be used a lot, we should create a model for it... Calling it "shared"
  - the "shared" folder will contain features or elements of this application which are shared across different features like the 
    ingredient, which will be used in the shopping-list and the recipe section.
*/
