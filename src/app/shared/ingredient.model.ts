// export class Ingredient {
//   public name: string;
//   public amount: number;
   
//   constructor(name: string, amount: number) {
//     this.name = name;
//     this.amount = amount;
//   }
// }
/* now, how should the ingredient look... ask self, what does it need to have, what do I need to use, etc...

    It will need:   - name 
                    - amount: how many items of this item you have...

    In the constructor, list out how you want to recieve the class above... You want to recieve them as arguements, name and amount.

    - Next, go into the shopping-list components and breakdown your Ingredient Array
*/
// ANOTHER WAY OF DEFINING THE CLASS 'INGREDIENT' IS MUCH EASIER, B E L O W:
export class Ingredient {
  constructor(public name: string, public amount: number) {}
}
